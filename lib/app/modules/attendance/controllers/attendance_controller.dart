import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/ui.dart';
import '../../../models/attendance_model.dart';
import '../../../repositories/attendance_repository.dart';
import '../../../routes/app_routes.dart';
import '../../../services/auth_service.dart';
import '../../book_ranem/controllers/book_ranem_controller.dart';
import 'attendances_controller.dart';

class AttendanceController extends GetxController {
  final attendance = Attendance().obs;
  GlobalKey<FormState> attendanceForm;
  final loading = false.obs;
  RxBool isEx = true.obs;
  var selectedGender = "Male".obs;
  List<String> genders = ["Male", "Female"];
  AttendanceRepository _attendanceRepository;

  AttendanceController() {
    _attendanceRepository = new AttendanceRepository();
  }

  @override
  void onInit() {
    isEx.value = isExist;
    super.onInit();
  }

  get isExist =>
      Get.find<AttendancesController>().attendances.isNotEmpty ? true : false;

  List<DropdownMenuItem<String>> getSelectGenderItem() {
    return genders.map((element) {
      return DropdownMenuItem(value: element, child: Text(element));
    }).toList();
  }

  void createAttendanceForm() async {
    Get.focusScope.unfocus();
    if (attendanceForm.currentState.validate()) {
      try {
        attendanceForm.currentState.save();
        loading.value = true;
        attendance.value.user_id = Get.find<AuthService>().user.value.id;
        Get.showSnackbar(
            Ui.SuccessSnackBar(message: "Attendance saved successfully".tr));
        await Get.find<AttendancesController>().refreshAttendances();
        await Get.find<BookRanemController>()
            .getAttendances(Get.find<AuthService>().user.value.id);
        Get.offAndToNamed(Routes.BOOK_RANEM);
      } catch (e) {
        Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
      } finally {
        loading.value = false;
      }
    } else {
      Get.showSnackbar(Ui.ErrorSnackBar(
          message: "There are errors in some fields please correct them!".tr));
    }
  }

  void resetAttendanceForm() {
    attendanceForm.currentState.reset();
  }

  void editAttendanceForm() async {
    Get.focusScope.unfocus();
    if (attendanceForm.currentState.validate()) {
      try {
        attendanceForm.currentState.save();
        loading.value = true;
        attendance.value.user_id = Get.find<AuthService>().user.value.id;
        Get.showSnackbar(
            Ui.SuccessSnackBar(message: "Attendance edited successfully".tr));
        await Get.find<AttendancesController>().refreshAttendances();
        Get.offAndToNamed(Routes.ROOT);
      } catch (e) {
        Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
      } finally {
        loading.value = false;
      }
    } else {
      Get.showSnackbar(Ui.ErrorSnackBar(
          message: "There are errors in some fields please correct them!".tr));
    }
  }

  Future<void> deleteAttendance(String AttendanceID) async {
    try {
      await _attendanceRepository.deleteAttendance(AttendanceID);
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }
}
