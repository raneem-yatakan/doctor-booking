import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/ui.dart';
import '../../../models/attendance_model.dart';
import '../../../repositories/attendance_repository.dart';
import '../../../services/auth_service.dart';

class AttendancesController extends GetxController {
  final attendances = <Attendance>[].obs;
  final page = 0.obs;
  final isLoading = true.obs;
  final isDone = false.obs;
  ScrollController scrollController = ScrollController();
  AttendanceRepository _attendanceRepository;

  AttendancesController() {
    _attendanceRepository = new AttendanceRepository();
  }

  @override
  void onInit() async {
    scrollController.addListener(() async {
      if (scrollController.position.pixels ==
              scrollController.position.maxScrollExtent &&
          !isDone.value) {
        await getAttendances(Get.find<AuthService>().user.value.id);
      }
    });
    super.onInit();
  }

  Future refreshAttendances({bool showMessage = false}) async {
    this.attendances.clear();
    page.value = 0;
    await getAttendances(Get.find<AuthService>().user.value.id);
    if (showMessage) {
      Get.showSnackbar(
          Ui.SuccessSnackBar(message: "Attendances refreshed successfully".tr));
    }
  }

  Future getAttendances(String UserID) async {
    try {
      isLoading.value = true;
      isDone.value = false;
      page.value++;
      List<Attendance> _attendances = [];
      _attendances =
          await _attendanceRepository.getWithUserId(UserID, page: page.value);
      if (_attendances.isNotEmpty) {
        attendances.addAll(_attendances);
      } else {
        isDone.value = true;
      }
    } catch (e) {
      isDone.value = true;
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    } finally {
      isLoading.value = false;
    }
  }
}
