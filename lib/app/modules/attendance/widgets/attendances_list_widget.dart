import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../providers/laravel_provider.dart';
import '../controllers/attendances_controller.dart';
import 'attendances_empty_list_widget.dart';
import 'attendances_list_item_widget.dart';
import 'attendances_list_loader_widget.dart';

class AttendancesListWidget extends GetView<AttendancesController> {
  AttendancesListWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (Get.find<LaravelApiClient>()
              .isLoading(task: 'getAttendancesWithUserId') &&
          controller.page.value == 1) {
        return AttendancesListLoaderWidget();
      } else if (controller.attendances.isEmpty) {
        return AttendancesEmptyListWidget();
      } else {
        return ListView.builder(
          padding: EdgeInsets.only(bottom: 10, top: 10),
          primary: false,
          shrinkWrap: true,
          itemCount: controller.attendances.length,
          itemBuilder: ((_, index) {
            if (index == controller.attendances.length) {
              return Obx(() {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new Center(
                    child: new Opacity(
                      opacity: controller.isLoading.value ? 1 : 0,
                      child: new CircularProgressIndicator(),
                    ),
                  ),
                );
              });
            } else {
              var _attendance = controller.attendances.elementAt(index);
              return AttendancesListItemWidget(attendance: _attendance);
            }
          }),
        );
      }
    });
  }
}
