import 'package:get/get.dart';

import '../../account/controllers/account_controller.dart';
import '../../appointments/controllers/appointment_controller.dart';
import '../../appointments/controllers/appointments_controller.dart';
import '../../attendance/controllers/attendances_controller.dart';
import '../../messages/controllers/messages_controller.dart';
import '../../ranem/controllers/ranem_controller.dart';
import '../controllers/root_controller.dart';

class RootBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RootController>(
      () => RootController(),
    );

    // Get.put(HomeController(), permanent: true);
    Get.put(RanemController(), permanent: true);
    Get.put(AppointmentsController(), permanent: true);
    Get.put(MessagesController(), permanent: true);

    Get.lazyPut<AppointmentController>(
      () => AppointmentController(),
    );
    Get.lazyPut<MessagesController>(
      () => MessagesController(),
    );
    Get.lazyPut<AccountController>(
      () => AccountController(),
    );

    Get.lazyPut<AttendancesController>(
      () => AttendancesController(),
    );
  }
}
