import 'package:get/get.dart';

import '../controllers/book_ranem_controller.dart';

class BookRanemBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BookRanemController>(
      () => BookRanemController(),
    );
  }
}
