import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart' show DateFormat;

import '../../../../common/ui.dart';
import '../../../models/appointment_model.dart';
import '../../../models/coupon_model.dart';
import '../../../models/ranem_model.dart';
import '../../../models/attendance_model.dart';
import '../../../repositories/appointment_repository.dart';
import '../../../repositories/ranem_repository.dart';
import '../../../repositories/attendance_repository.dart';
import '../../../routes/app_routes.dart';
import '../../../services/auth_service.dart';
import '../../appointments/controllers/appointments_controller.dart';
import '../../global_widgets/tab_bar_widget.dart';

class BookRanemController extends GetxController {
  final scheduled = false.obs;
  final onlineConsultation = true.obs;
  final atYatakan = false.obs;
  final atAddress = false.obs;
  final List<bool> appointmentTypes = <bool>[true, false, false];
  final appointment = Appointment().obs;
  final attendances = <Attendance>[].obs;
  final morningTimes = [].obs;
  final afternoonTimes = [].obs;
  final eveningTimes = [].obs;
  final nightTimes = [].obs;
  AppointmentRepository _appointmentRepository;
  AttendanceRepository _attendanceRepository;
  RanemRepository _ranemRepository;
  DatePickerController datePickerController = DatePickerController();
  var acceptedAppointmentsList = [];

  BookRanemController() {
    _appointmentRepository = AppointmentRepository();
    _ranemRepository = RanemRepository();
    _attendanceRepository = AttendanceRepository();
  }

  @override
  void onInit() async {
    super.onInit();
    final _ranem = (Get.arguments['ranem'] as Ranem);
    this.appointment.value = Appointment(
      appointmentAt: DateTime.now(),
      ranem: _ranem,
      yatakan: _ranem.yatakan,
      taxes: _ranem.yatakan.taxes,
      online: true,
      user: Get.find<AuthService>().user.value,
      coupon: new Coupon(),
    );

    await getAcceptedAppointments();
    await getTimes();
    await getAttendances(Get.find<AuthService>().user.value.id);
    super.onInit();
  }

  Future<void> getAcceptedAppointments() async {
    acceptedAppointmentsList = await _appointmentRepository.all2('4', page: 1);
  }

  void toggleAtYatakan(value) {
    atYatakan.value = value;
    atAddress.value = false;
    onlineConsultation.value = false;
    appointment.update((val) {
      val.online = false;
    });
  }

  void toggleAtAddress(value) {
    atAddress.value = value;
    atYatakan.value = false;
    onlineConsultation.value = false;
    appointment.update((val) {
      val.online = false;
    });
  }

  void toggleOnline(value) {
    appointment.update((val) {
      val.online = true;
    });
    atYatakan.value = false;
    atAddress.value = false;
    onlineConsultation.value = value;
  }

  TextStyle getTextTheme(bool selected) {
    if (selected) {
      return Get.textTheme.bodyText2
          .merge(TextStyle(color: Get.theme.primaryColor));
    }
    return Get.textTheme.bodyText2;
  }

  Color getColor(bool selected) {
    if (selected) {
      return Get.theme.colorScheme.secondary;
    }
    return null;
  }

  void createAppointment() async {
    try {
      await _appointmentRepository.add(appointment.value);
      Get.find<AppointmentsController>().currentStatus.value =
          Get.find<AppointmentsController>().getStatusByOrder(1).id;
      if (Get.isRegistered<TabBarController>(tag: 'appointments')) {
        Get.find<TabBarController>(tag: 'appointments').selectedId.value =
            Get.find<AppointmentsController>().getStatusByOrder(1).id;
      }
      Get.toNamed(Routes.CONFIRMATION);
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }

  Future getAttendances(String UserId) async {
    try {
      if (Get.find<AuthService>().isAuth) {
        attendances
            .assignAll(await _attendanceRepository.getAllWithUserId(UserId));
      }
    } on Exception catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }

  Future<List> checkTimes(List times, List accepted) async {
    List _list = [];
    for (var time in times) {
      for (var acc in accepted) {
        var localDateTime = DateTime.parse(time[0]).toLocal();
        if (localDateTime == acc.appointmentAt) {
          time[1] = false;
        }
      }
      _list.add(time);
    }

    return _list;
  }

  Future getTimes({DateTime date}) async {
    try {
      nightTimes.clear();
      morningTimes.clear();
      afternoonTimes.clear();
      eveningTimes.clear();
      List<dynamic> times = await _ranemRepository.getAvailabilityHours(
          this.appointment.value.ranem.id, date ?? DateTime.now());
      times.sort((e1, e2) {
        final _localDateTime1 = DateTime.parse(e1.elementAt(0)).toLocal();
        final hours1 = int.tryParse(DateFormat('HH').format(_localDateTime1));
        final _localDateTime2 = DateTime.parse(e2.elementAt(0)).toLocal();
        final hours2 = int.tryParse(DateFormat('HH').format(_localDateTime2));
        return hours1.compareTo(hours2);
      });

      times = await checkTimes(times, acceptedAppointmentsList);
      nightTimes.assignAll(times.sublist(22)); //9 4
      morningTimes.assignAll(times.sublist(7, 12)); //5 12
      afternoonTimes.assignAll(times.sublist(12, 18)); //12 5
      eveningTimes.assignAll(times.sublist(18, 22)); //5 9
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }

  void validateCoupon() async {
    try {
      Coupon _coupon = await _appointmentRepository.coupon(appointment.value);
      appointment.update((val) {
        val.coupon = _coupon;
      });
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }

  String getValidationMessage() {
    if (appointment.value.coupon?.id == null) {
      return null;
    } else {
      if (appointment.value.coupon.id == '') {
        return "Invalid Coupon Code".tr;
      } else {
        return null;
      }
    }
  }

  Future<Null> showMyDatePicker(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: appointment.value.appointmentAt.add(Duration(days: 1)),
      firstDate: DateTime.now().add(Duration(days: 1)),
      lastDate: DateTime(2101),
      locale: Get.locale,
      builder: (BuildContext context, Widget child) {
        return child.paddingAll(10);
      },
    );
    if (picked != null) {
      appointment.update((val) {
        val.appointmentAt = DateTime(picked.year, picked.month, picked.day,
            val.appointmentAt.hour, val.appointmentAt.minute);
        ;
      });
    }
  }

  Future<Null> showMyTimePicker(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.fromDateTime(appointment.value.appointmentAt),
      builder: (BuildContext context, Widget child) {
        return child.paddingAll(10);
      },
    );
    if (picked != null) {
      appointment.update((val) {
        val.appointmentAt = DateTime(
                appointment.value.appointmentAt.year,
                appointment.value.appointmentAt.month,
                appointment.value.appointmentAt.day)
            .add(Duration(minutes: picked.minute + picked.hour * 60));
      });
    }
  }

  void selectAttendance(Attendance attendance) async {
    appointment.update((val) {
      if (val.attendance == null) {
        val.attendance = attendance;
      } else {
        val.attendance = null;
      }
    });
    // if (appointment.value.appointmentAt != null) {
    //   await getTimes(date: appointment.value.appointmentAt);
    // }
  }

  void selectAppointmentType(value) {
    // appointment.update((val) {
    //   if (val.type == null) {
    //     val.type = value;
    //   } else {
    //     val.type = null;
    //   }
    // });
  }

  bool isCheckedAttendance(Attendance attendance) {
    return (appointment.value.attendance?.id ?? '0') == attendance.id;
  }

  TextStyle getTitleTheme(Attendance attendance) {
    if (isCheckedAttendance(attendance)) {
      return Get.textTheme.bodyText2
          .merge(TextStyle(color: Get.theme.colorScheme.secondary));
    }
    return Get.textTheme.bodyText2;
  }
}
