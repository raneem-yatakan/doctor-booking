import 'dart:async';

import 'package:get/get.dart';

import '../../../../common/ui.dart';
import '../../../models/appointment_model.dart';
import '../../../models/review_model.dart';
import '../../../repositories/appointment_repository.dart';
import '../../../services/auth_service.dart';
import '../../root/controllers/root_controller.dart';

class RatingController extends GetxController {
  final appointment = Appointment().obs;
  final ranemReview = new Review(rate: 0).obs;
  // final yatakanReview = new Review(rate: 0).obs;
  AppointmentRepository _appointmentRepository;

  RatingController() {
    _appointmentRepository = new AppointmentRepository();
  }

  @override
  void onInit() {
    appointment.value = Get.arguments as Appointment;
    ranemReview.value.user = Get.find<AuthService>().user.value;
    ranemReview.value.ranem = appointment.value.ranem;
    // yatakanReview.value.yatakan = appointment.value.yatakan;
    super.onInit();
  }

  Future addRanemReview() async {
    try {
      if (ranemReview.value.rate < 1) {
        Get.showSnackbar(Ui.ErrorSnackBar(
            message: "Please rate this ranem by clicking on the stars".tr));
        return;
      }
      if (ranemReview.value.review == null ||
          ranemReview.value.review.isEmpty) {
        //Get.showSnackbar(Ui.ErrorSnackBar(message: "Tell us somethings about this Counselor".tr));
        return;
      }
      await _appointmentRepository.addRanemReview(ranemReview.value);
      Get.showSnackbar(Ui.SuccessSnackBar(
          message: "Thank you! your review has been added".tr));
      Timer(Duration(seconds: 2), () {
        Get.find<RootController>().changePage(0);
      });
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }

  // Future addYatakanReview() async {
  //   try {
  //     if (yatakanReview.value.rate < 1) {
  //       Get.showSnackbar(Ui.ErrorSnackBar(message: "Please rate this ranem by clicking on the stars".tr));
  //       return;
  //     }
  //     if (yatakanReview.value.review == null || yatakanReview.value.review.isEmpty) {
  //       Get.showSnackbar(Ui.ErrorSnackBar(message: "Tell us somethings about this yatakan".tr));
  //       return;
  //     }
  //     await _appointmentRepository.addYatakanReview(yatakanReview.value);
  //     Get.showSnackbar(Ui.SuccessSnackBar(message: "Thank you! your review has been added".tr));
  //     Timer(Duration(seconds: 2), () {
  //       Get.find<RootController>().changePage(0);
  //     });
  //   } catch (e) {
  //     Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
  //   }
  // }

}
