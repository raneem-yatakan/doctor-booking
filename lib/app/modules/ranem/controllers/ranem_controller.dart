import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/ui.dart';
import '../../../models/experience_model.dart';
import '../../../models/favorite_model.dart';
import '../../../models/message_model.dart';
import '../../../models/option_model.dart';
import '../../../models/ranem_model.dart';
import '../../../models/review_model.dart';
import '../../../models/user_model.dart';
import '../../../repositories/ranem_repository.dart';
import '../../../routes/app_routes.dart';
import '../../../services/auth_service.dart';
import '../../favorites/controllers/favorites_controller.dart';

class RanemController extends GetxController {
  final ranem = Ranem().obs;
  List ranems = [];
  final reviews = <Review>[].obs;
  final experiences = <Experience>[].obs;
  final currentSlide = 0.obs;
  final heroTag = ''.obs;
  RanemRepository _ranemRepository;

  RanemController() {
    _ranemRepository = new RanemRepository();
  }

  @override
  void onInit() async {
    // var arguments = Get.arguments as Map<String, dynamic>;
    // ranem.value = arguments['ranem'] as Ranem;
    // heroTag.value = arguments['heroTag'] as String;

    // await getRecommendedRanems();

    // ranem.value = ranems[0];
    ranem.value = await _ranemRepository.get('1');
    // await getRanem();
    super.onInit();
  }

  @override
  void onReady() async {
    await refreshRanem();
    super.onReady();
  }

  Future refreshRanem({bool showMessage = false}) async {
    await getRanem();
    await getReviews();
    await getExperiences();
    if (showMessage) {
      Get.showSnackbar(Ui.SuccessSnackBar(
          message: ranem.value.name + " " + "page refreshed successfully".tr));
    }
  }

  Future getRecommendedRanems() async {
    try {
      ranems.assignAll(await _ranemRepository.getRecommended());
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }

  Future getRanem() async {
    try {
      ranem.value = await _ranemRepository.get(ranem.value.id);
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }

  Future getReviews() async {
    try {
      reviews.assignAll(await _ranemRepository.getReviews(ranem.value.id));
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }

  Future getExperiences() async {
    try {
      experiences
          .assignAll(await _ranemRepository.getExperiences(ranem.value.id));
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }

  Future addToFavorite() async {
    try {
      Favorite _favorite = new Favorite(
        ranem: this.ranem.value,
        userId: Get.find<AuthService>().user.value.id,
      );
      await _ranemRepository.addFavorite(_favorite);
      ranem.update((val) {
        val.isFavorite = true;
      });
      if (Get.isRegistered<FavoritesController>()) {
        Get.find<FavoritesController>().refreshFavorites();
      }
      Get.showSnackbar(Ui.SuccessSnackBar(
          message: this.ranem.value.name + " Added to favorite list".tr));
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }

  Future removeFromFavorite() async {
    try {
      Favorite _favorite = new Favorite(
        ranem: this.ranem.value,
        userId: Get.find<AuthService>().user.value.id,
      );
      await _ranemRepository.removeFavorite(_favorite);
      ranem.update((val) {
        val.isFavorite = false;
      });
      if (Get.isRegistered<FavoritesController>()) {
        Get.find<FavoritesController>().refreshFavorites();
      }
      Get.showSnackbar(Ui.SuccessSnackBar(
          message: this.ranem.value.name + " Removed from favorite list".tr));
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }

  TextStyle getTitleTheme(Option option) {
    if (option.checked.value) {
      return Get.textTheme.bodyText2
          .merge(TextStyle(color: Get.theme.colorScheme.secondary));
    }
    return Get.textTheme.bodyText2;
  }

  TextStyle getSubTitleTheme(Option option) {
    if (option.checked.value) {
      return Get.textTheme.caption
          .merge(TextStyle(color: Get.theme.colorScheme.secondary));
    }
    return Get.textTheme.caption;
  }

  Color getColor(Option option) {
    if (option.checked.value) {
      return Get.theme.colorScheme.secondary.withOpacity(0.1);
    }
    return null;
  }

  void startChat() {
    var _ranems = <User>[].obs;
    print(ranem.value.user);
    _ranems.add(ranem.value.user);
    Message _message = new Message(_ranems, name: ranem.value.name);
    Get.toNamed(Routes.CHAT, arguments: _message);
  }
}
