import 'package:get/get.dart';

import '../controllers/ranem_controller.dart';

class RanemBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RanemController>(
      () => RanemController(),
    );
  }
}
