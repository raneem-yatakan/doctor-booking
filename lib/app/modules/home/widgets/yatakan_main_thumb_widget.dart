

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../models/yatakan_model.dart';

class YatakanMainThumbWidget extends StatelessWidget {
  const YatakanMainThumbWidget({
    Key key,
    @required Yatakan yatakan,
  })  : _yatakan = yatakan,
        super(key: key);

  final Yatakan _yatakan;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Hero(
          tag: 'recommended_carousel' + _yatakan.id,
          child: ClipRRect(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            child: CachedNetworkImage(
              height: 150,
              width: double.infinity,
              fit: BoxFit.cover,
              imageUrl: _yatakan.firstImageUrl,
              placeholder: (context, url) => Image.asset(
                'assets/img/loading.gif',
                fit: BoxFit.cover,
                width: double.infinity,
                height: 100,
              ),
              errorWidget: (context, url, error) => Icon(Icons.error_outline),
            ),
          ),
        ),
        //YatakanLevelBadgeWidget(yatakan: _yatakan),
      ],
    );
  }
}
