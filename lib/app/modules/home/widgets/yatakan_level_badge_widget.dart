
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
//
// import '../../../models/yatakan_model.dart';
//
// class YatakanLevelBadgeWidget extends StatelessWidget {
//   const YatakanLevelBadgeWidget({
//     Key key,
//     @required Yatakan yatakan,
//   })  : _yatakan = yatakan,
//         super(key: key);
//
//   final Yatakan _yatakan;
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: EdgeInsetsDirectional.only(start: 12, top: 10),
//       child: Text(_yatakan.type?.name ?? '',
//           maxLines: 1,
//           style: Get.textTheme.bodyText2.merge(
//             TextStyle(color: Get.theme.primaryColor, height: 1.4, fontSize: 10),
//           ),
//           softWrap: false,
//           textAlign: TextAlign.center,
//           overflow: TextOverflow.fade),
//       decoration: BoxDecoration(
//         color: Get.theme.colorScheme.secondary.withOpacity(0.8),
//         borderRadius: BorderRadius.circular(8),
//       ),
//       padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
//     );
//   }
// }
