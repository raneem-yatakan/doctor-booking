import 'package:get/get.dart';

import '../../../../common/ui.dart';
import '../../../models/ranem_model.dart';
import '../../../models/slide_model.dart';
import '../../../models/speciality_model.dart';
import '../../../models/yatakan_model.dart';
import '../../../repositories/ranem_repository.dart';
import '../../../repositories/slider_repository.dart';
import '../../root/controllers/root_controller.dart';

class HomeController extends GetxController {
  SliderRepository _sliderRepo;
  RanemRepository _ranemRepository;

  final slider = <Slide>[].obs;
  final currentSlide = 0.obs;

  final yatakans = <Yatakan>[].obs;
  final ranems = <Ranem>[].obs;
  final specialities = <Speciality>[].obs;
  final featured = <Speciality>[].obs;

  HomeController() {
    _sliderRepo = new SliderRepository();
    _ranemRepository = new RanemRepository();
  }

  @override
  Future<void> onInit() async {
    await refreshHome();
    super.onInit();
  }

  Future refreshHome({bool showMessage = false}) async {
    await getSlider();
    await getRecommendedRanems();
    Get.find<RootController>().getNotificationsCount();
    if (showMessage) {
      Get.showSnackbar(
          Ui.SuccessSnackBar(message: "Home page refreshed successfully".tr));
    }
  }



  Future getSlider() async {
    try {
      slider.assignAll(await _sliderRepo.getHomeSlider());
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }

 


  Future getRecommendedRanems() async {
    try {
      ranems.assignAll(await _ranemRepository.getRecommended());
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }


}
