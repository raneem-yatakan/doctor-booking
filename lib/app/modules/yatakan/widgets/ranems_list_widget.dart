import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../providers/laravel_provider.dart';
import '../controllers/ranems_controller.dart';
import 'ranems_empty_list_widget.dart';
import 'ranems_list_item_widget.dart';
import 'ranems_list_loader_widget.dart';

class RanemsListWidget extends GetView<RanemsController> {
  RanemsListWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (Get.find<LaravelApiClient>().isLoading(tasks: [
            'getYatakanRanems',
            'getYatakanPopularRanems',
            'getYatakanMostRatedRanems',
            'getYatakanAvailableRanems',
            'getYatakanFeaturedRanems'
          ]) &&
          controller.page == 1) {
        return ServicesListLoaderWidget();
      } else if (controller.ranems.isEmpty) {
        return RanemsEmptyListWidget();
      } else {
        return ListView.builder(
          padding: EdgeInsets.only(bottom: 10, top: 10),
          primary: false,
          shrinkWrap: true,
          itemCount: controller.ranems.length + 1,
          itemBuilder: ((_, index) {
            if (index == controller.ranems.length) {
              return Obx(() {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new Center(
                    child: new Opacity(
                      opacity: controller.isLoading.value ? 1 : 0,
                      child: new CircularProgressIndicator(),
                    ),
                  ),
                );
              });
            } else {
              var _ranem = controller.ranems.elementAt(index);
              return RanemsListItemWidget(ranem: _ranem);
            }
          }),
        );
      }
    });
  }
}
