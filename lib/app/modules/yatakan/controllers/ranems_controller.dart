import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../models/ranem_model.dart';

enum SpecialityFilter { ALL, AVAILABILITY, RATING, FEATURED, POPULAR }

class RanemsController extends GetxController {
  final ranems = <Ranem>[].obs;
  final page = 0.obs;
  final isLoading = true.obs;
  final isDone = false.obs;
  ScrollController scrollController = ScrollController();

  RanemsController() {}

  @override
  Future<void> onInit() async {
    super.onInit();
  }

  @override
  void onClose() {
    scrollController.dispose();
  }
}
