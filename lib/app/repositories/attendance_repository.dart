import 'package:get/get.dart';

import '../models/attendance_model.dart';
import '../providers/laravel_provider.dart';

class AttendanceRepository {
  LaravelApiClient _laravelApiClient;

  AttendanceRepository() {
    _laravelApiClient = Get.find<LaravelApiClient>();
  }

  Future<Attendance> get(String id) {
    return _laravelApiClient.getAttendance(id);
  }

  Future<List<Attendance>> getWithUserId(String UserId, {int page}) {
    return _laravelApiClient.getAttendancesWithUserId(UserId, page);
  }

  Future<List<Attendance>> getAllWithUserId(String UserId) {
    return _laravelApiClient.getAllAttendancesWithUserId(UserId);
  }

  Future<Attendance> update(Attendance attendance) {
    _laravelApiClient = Get.find<LaravelApiClient>();

    return _laravelApiClient.updateAttendance(attendance);
  }

  Future<Attendance> create(Attendance attendance) {
    _laravelApiClient = Get.find<LaravelApiClient>();
    return _laravelApiClient.createAttendance(attendance);
  }

  Future<void> deleteAttendance(String AttendanceID) async {
    _laravelApiClient = Get.find<LaravelApiClient>();

    await _laravelApiClient.deleteAttendance(AttendanceID);
  }
}
