import 'package:get/get.dart';

import '../models/ranem_model.dart';
import '../models/experience_model.dart';
import '../models/favorite_model.dart';
import '../models/review_model.dart';
import '../providers/laravel_provider.dart';

class RanemRepository {
  LaravelApiClient _laravelApiClient;

  RanemRepository() {
    this._laravelApiClient = Get.find<LaravelApiClient>();
  }

  Future<List<Ranem>> getAllWithPagination(String specialityId, {int page}) {
    return _laravelApiClient.getAllRanemsWithPagination(specialityId, page);
  }

  Future<List<Ranem>> search(String keywords, List<String> specialities,
      {int page = 1}) {
    return _laravelApiClient.searchRanems(keywords, specialities, page);
  }

  Future<List<Favorite>> getFavorites() {
    return _laravelApiClient.getFavoritesRanems();
  }

  Future<List<Experience>> getExperiences(String ranemId) {
    return _laravelApiClient.getRanemExperiences(ranemId);
  }

  Future<Favorite> addFavorite(Favorite favorite) {
    return _laravelApiClient.addFavoriteRanem(favorite);
  }

  Future<bool> removeFavorite(Favorite favorite) {
    return _laravelApiClient.removeFavoriteRanem(favorite);
  }

  Future<List<Ranem>> getRecommended() {
    return _laravelApiClient.getRecommendedRanems();
  }

  Future<List<Ranem>> getMostRated(String specialityId, {int page}) {
    return _laravelApiClient.getMostRatedRanems(specialityId, page);
  }

  Future<List<Ranem>> getAvailable(String specialityId, {int page}) {
    return _laravelApiClient.getAvailableRanems(specialityId, page);
  }

  Future<Ranem> get(String id) {
    return _laravelApiClient.getRanem(id);
  }

  Future<List<Review>> getReviews(String ranemId) {
    return _laravelApiClient.getRanemReviews(ranemId);
  }

  Future<List> getAvailabilityHours(String ranemId, DateTime date) {
    return _laravelApiClient.getAvailabilityHours(ranemId, date);
  }
}
