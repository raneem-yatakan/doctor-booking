import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

import '../models/appointment_model.dart';
import '../models/speciality_model.dart';
import '../models/yatakan_model.dart';
import '../models/ranem_model.dart';
import '../models/faq_category_model.dart';
import '../models/notification_model.dart';
import '../models/review_model.dart';
import '../models/setting_model.dart';
import '../models/slide_model.dart';
import '../models/user_model.dart';
import '../services/global_service.dart';

class MockApiClient {
  final _globalService = Get.find<GlobalService>();

  String get baseUrl => _globalService.global.value.mockBaseUrl;

  final Dio httpClient;
  final Options _options = buildCacheOptions(Duration(days: 3), forceRefresh: true);

  MockApiClient({@required this.httpClient}) {
    httpClient.interceptors.add(DioCacheManager(CacheConfig(baseUrl: baseUrl)).interceptor);
  }

  Future<List<User>> getAllUsers() async {
    var response = await httpClient.get(baseUrl + "users/all.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<User>((obj) => User.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Slide>> getHomeSlider() async {
    var response = await httpClient.get(baseUrl + "slides/home.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['results'].map<Slide>((obj) => Slide.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<User> getLogin() async {
    var response = await httpClient.get(baseUrl + "users/user.json", options: _options);
    if (response.statusCode == 200) {
      return User.fromJson(response.data['data']);
    } else {
      throw new Exception(response.statusMessage);
    }
  }



  Future<List<Ranem>> getRecommendedRanems() async {
    var response = await httpClient.get(baseUrl + "ranems/recommended.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Ranem>> getAllRanems() async {
    var response = await httpClient.get(baseUrl + "ranems/all.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Ranem>> getAllRanemsWithPagination(int page) async {
    var response = await httpClient.get(baseUrl + "ranems/all_page_$page.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Ranem>> getFavoritesRanems() async {
    var response = await httpClient.get(baseUrl + "ranems/favorites.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<Ranem> getRanem(String id) async {
    var response = await httpClient.get(baseUrl + "ranems/all.json", options: _options);
    if (response.statusCode == 200) {
      List<Ranem> _list = response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
      return _list.firstWhere((element) => element.id == id, orElse: () => new Ranem());
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<Yatakan> getYatakan(String yatakanId) async {
    var response = await httpClient.get(baseUrl + "providers/eprovider.json", options: _options);
    if (response.statusCode == 200) {
      return Yatakan.fromJson(response.data['data']);
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Review>> getYatakanReviews(String yatakanId) async {
    var response = await httpClient.get(baseUrl + "providers/reviews.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Review>((obj) => Review.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Ranem>> getYatakanFeaturedRanems(String yatakanId) async {
    var response = await httpClient.get(baseUrl + "ranems/featured.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  // getYatakanMostRatedRanems
  Future<List<Ranem>> getYatakanPopularRanems(String yatakanId) async {
    var response = await httpClient.get(baseUrl + "ranems/popular.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Ranem>> getYatakanAvailableRanems(String yatakanId) async {
    var response = await httpClient.get(baseUrl + "ranems/all.json", options: _options);
    if (response.statusCode == 200) {
      List<Ranem> _ranems = response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
      _ranems = _ranems.where((_ranem) {
        return _ranem.available;
      }).toList();
      return _ranems;
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Ranem>> getYatakanMostRatedRanems(String yatakanId) async {
    var response = await httpClient.get(baseUrl + "ranems/all.json", options: _options);
    if (response.statusCode == 200) {
      List<Ranem> _ranems = response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
      _ranems.sort((s1, s2) {
        return s2.rate.compareTo(s1.rate);
      });
      return _ranems;
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Ranem>> getYatakanRanemsWithPagination(String yatakanId, int page) async {
    var response = await httpClient.get(baseUrl + "ranems/all_page_$page.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Review>> getRanemReviews(String ranemId) async {
    var response = await httpClient.get(baseUrl + "ranems/reviews.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Review>((obj) => Review.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Ranem>> getFeaturedRanems() async {
    var response = await httpClient.get(baseUrl + "ranems/featured.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Ranem>> getPopularRanems() async {
    var response = await httpClient.get(baseUrl + "ranems/popular.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Ranem>> getMostRatedRanems() async {
    var response = await httpClient.get(baseUrl + "ranems/all.json", options: _options);
    if (response.statusCode == 200) {
      List<Ranem> _ranems = response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
      _ranems.sort((s1, s2) {
        return s2.rate.compareTo(s1.rate);
      });
      return _ranems;
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Ranem>> getAvailableRanems() async {
    var response = await httpClient.get(baseUrl + "ranems/all.json", options: _options);
    if (response.statusCode == 200) {
      List<Ranem> _ranems = response.data['data'].map<Ranem>((obj) => Ranem.fromJson(obj)).toList();
      _ranems = _ranems.where((_ranem) {
        return _ranem.available;
      }).toList();
      return _ranems;
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Speciality>> getAllSpecialities() async {
    var response = await httpClient.get(baseUrl + "specialities/all2.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['results'].map<Speciality>((obj) => Speciality.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Speciality>> getAllWithSubSpecialities() async {
    var response = await httpClient.get(baseUrl + "specialities/subspecialities.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['results'].map<Speciality>((obj) => Speciality.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Speciality>> getFeaturedSpecialities() async {
    var response = await httpClient.get(baseUrl + "specialities/featured.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Speciality>((obj) => Speciality.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Appointment>> getAppointments(int page) async {
    var response = await httpClient.get(baseUrl + "tasks/all_page_$page.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Appointment>((obj) => Appointment.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<Appointment> getAppointment(String appointmentId) async {
    var response = await httpClient.get(baseUrl + "tasks/all.json", options: _options);
    if (response.statusCode == 200) {
      List<Appointment> _appointments = response.data['data'].map<Appointment>((obj) => Appointment.fromJson(obj)).toList();
      return _appointments.firstWhere((element) => element.id == appointmentId, orElse: () => new Appointment());
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<Notification>> getNotifications() async {
    var response = await httpClient.get(baseUrl + "notifications/all.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<Notification>((obj) => Notification.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<List<FaqCategory>> getSpecialitiesWithFaqs() async {
    var response = await httpClient.get(baseUrl + "help/faqs.json", options: _options);
    if (response.statusCode == 200) {
      return response.data['data'].map<FaqCategory>((obj) => FaqCategory.fromJson(obj)).toList();
    } else {
      throw new Exception(response.statusMessage);
    }
  }

  Future<Setting> getSettings() async {
    var response = await httpClient.get(baseUrl + "settings/all.json", options: _options);
    if (response.statusCode == 200) {
      return Setting.fromJson(response.data['data']);
    } else {
      throw new Exception(response.statusMessage);
    }
  }
}
