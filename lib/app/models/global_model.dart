import 'parents/model.dart';

class Global extends Model {
  String mockBaseUrl;
  String laravelBaseUrl;
  String ranemEmail;
  String apiPath;
  int received;
  int accepted;
  int onTheWay;
  int ready;
  int inProgress;
  int done;
  int failed;

  Global(
      {this.mockBaseUrl, this.laravelBaseUrl, this.apiPath, this.ranemEmail});

  Global.fromJson(Map<String, dynamic> json) {
    mockBaseUrl = json['mock_base_url'].toString();
    laravelBaseUrl = json['laravel_base_url'].toString();
    ranemEmail = json['ranem_email'].toString();
    apiPath = json['api_path'].toString();
    received = intFromJson(json, 'received');
    accepted = intFromJson(json, 'accepted');
    onTheWay = intFromJson(json, 'on_the_way');
    ready = intFromJson(json, 'ready');
    inProgress = intFromJson(json, 'in_progress');
    done = intFromJson(json, 'done');
    failed = intFromJson(json, 'failed');
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['mock_base_url'] = this.mockBaseUrl;
    data['laravel_base_url'] = this.laravelBaseUrl;
    data['ranem_email'] = this.ranemEmail;
    data['api_path'] = this.apiPath;
    return data;
  }
}
