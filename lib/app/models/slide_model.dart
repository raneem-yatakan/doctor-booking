import 'package:flutter/material.dart';

import 'media_model.dart';
import 'parents/model.dart';
import 'ranem_model.dart';
import 'yatakan_model.dart';

class Slide extends Model {
  int order;
  String text;
  String button;
  String textPosition;
  Color textColor;
  Color buttonColor;
  Color backgroundColor;
  Color indicatorColor;
  Media image;
  String imageFit;
  Ranem ranem;
  Yatakan yatakan;
  bool enabled;

  Slide({
    this.order,
    this.text,
    this.button,
    this.textPosition,
    this.textColor,
    this.buttonColor,
    this.backgroundColor,
    this.indicatorColor,
    this.image,
    this.imageFit,
    this.ranem,
    this.yatakan,
    this.enabled,
  });

  Slide.fromJson(Map<String, dynamic> json) {
    super.fromJson(json);
    order = intFromJson(json, 'order');
    text = transStringFromJson(json, 'text');
    button = transStringFromJson(json, 'button');
    textPosition = stringFromJson(json, 'text_position');
    textColor = colorFromJson(json, 'text_color');
    buttonColor = colorFromJson(json, 'button_color');
    backgroundColor = colorFromJson(json, 'background_color');
    indicatorColor = colorFromJson(json, 'indicator_color');
    image = mediaFromJson(json, 'image');
    imageFit = stringFromJson(json, 'image_fit');
    ranem = json['ranem_id'] != null ? Ranem(id: json['ranem_id'].toString()) : null;
    yatakan = json['yatakan_id'] != null ? Yatakan(id: json['yatakan_id'].toString()) : null;
    enabled = boolFromJson(json, 'enabled');
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['text'] = this.text;
    return data;
  }
}
