import 'parents/model.dart';
import 'ranem_model.dart';

class Favorite extends Model {
  String id;
  Ranem ranem;
  String userId;

  Favorite({this.id, this.ranem, this.userId});

  Favorite.fromJson(Map<String, dynamic> json) {
    super.fromJson(json);
    ranem = objectFromJson(json, 'ranem', (v) => Ranem.fromJson(v));
    userId = stringFromJson(json, 'user_id');
  }

  Map<String, dynamic> toJson() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["ranem_id"] = ranem.id;
    map["user_id"] = userId;
    return map;
  }
}
