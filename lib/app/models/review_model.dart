/*
 * Copyright (c) 2020 .
 */
import 'parents/model.dart';
import 'ranem_model.dart';
import 'user_model.dart';
import 'yatakan_model.dart';

class Review extends Model {
  String id;
  double rate;
  String review;
  DateTime createdAt;
  User user;
  Ranem ranem;
  Yatakan yatakan;

  Review({this.id, this.rate, this.review, this.createdAt, this.user, this.ranem,this.yatakan});

  Review.fromJson(Map<String, dynamic> json) {
    super.fromJson(json);
    rate = doubleFromJson(json, 'rate');
    review = stringFromJson(json, 'review');
    createdAt = dateFromJson(json, 'created_at', defaultValue: DateTime.now().toLocal());
    user = objectFromJson(json, 'user', (v) => User.fromJson(v));
    ranem = objectFromJson(json, 'ranem', (v) => Ranem.fromJson(v));
    yatakan = objectFromJson(json, 'yatakan', (v) => Yatakan.fromJson(v));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['rate'] = this.rate;
    data['review'] = this.review;
    data['created_at'] = this.createdAt;
    if (this.user != null) {
      data['user_id'] = this.user.id;
    }
    if (this.ranem != null) {
      data['ranem_id'] = this.ranem.id;
    }
    if (this.yatakan != null) {
      data['yatakan_id'] = this.yatakan.id;
    }
    return data;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is Review &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          rate == other.rate &&
          review == other.review &&
          createdAt == other.createdAt &&
          user == other.user &&
          ranem == other.ranem &&
          yatakan == other.yatakan;

  @override
  int get hashCode => super.hashCode ^ id.hashCode ^ rate.hashCode ^ review.hashCode ^ createdAt.hashCode ^ user.hashCode ^ ranem.hashCode ^ yatakan.hashCode;
}
