class Routes {
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const FORGOT_PASSWORD = '/forgot_password';
  static const PHONE_VERIFICATION = '/phone_verification';

  static const ROOT = '/root';
  static const RATING = '/rating';
  static const CHAT = '/chat';

  static const SETTINGS = '/settings';
  static const SETTINGS_THEME_MODE = '/settings/theme_mode';
  static const SETTINGS_LANGUAGE = '/settings/language';
  static const SETTINGS_ADDRESSES = '/settings/addresses';
  static const SETTINGS_ADDRESS_PICKER = '/settings/address_picker';

  static const PROFILE = '/profile';
  static const SPECIALITY = '/speciality';
  static const SPECIALITIES = '/specialities';
  static const MAPS = '/maps';
  static const SUB_SPECIALITY = '/sub_speciality';
  static const RANEM = '/ranem';
  static const BOOK_RANEM = '/book_ranem';
  static const APPOINTMENT_SUMMARY = '/appointment_summary';
  static const CHECKOUT = '/checkout';
  static const CONFIRMATION = '/confirmation';
  static const SEARCH = '/search';
  static const NOTIFICATIONS = '/notifications';
  static const FAVORITES = '/favorites';
  static const HELP = '/help';
  static const PRIVACY = '/privacy';
  static const YATAKAN = '/yatakan';
  //static const ATTENDANCES = '/attendances';
  static const ATTENDANCE = '/attendance';
  static const YATAKAN_RANEMS = '/yatakan/ranems';
  static const APPOINTMENT = '/appointment';
  static const PAYPAL = '/paypal';
  static const RAZORPAY = '/razorpay';
  static const STRIPE = '/stripe';
  static const STRIPE_FPX = '/stripefpx';
  static const PAYSTACK = '/paystack';
  static const PAYMONGO = '/paymongo';
  static const FLUTTERWAVE = '/flutterwave';
  static const CASH = '/cash';
  static const WALLET = '/wallet';
  static const CUSTOM_PAGES = '/custom_pages';
  static const GALLERY = '/gallery';
  static const WALLETS = '/wallets';
  static const WALLET_FORM = '/wallet_form';
}
